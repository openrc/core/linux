# Maintainer: OpenStage Linux <openstagelinux@gmail.com>

#######################
# BASED ON:
# ArchLinux kernel - https://github.com/archlinux/svntogit-packages/tree/packages/linux/trunk
# AUR Linux-bore - https://aur.archlinux.org/packages/linux-bore
#######################

## Enable it for compiling with AOCC-LLVM/CLANG-LLVM and THINLTO
_use_aocc=y
_use_clang=
# Enable it for using the LLVM CFI PATCH for a better security
_use_cfi=y


# Select between Kernel.org or Archlinux kernel sources
# Two optios "arch" and "official"
_linux_source="arch"

# NUMA is optimized for multi-socket motherboards.
# A single multi-core CPU actually runs slower with NUMA enabled.
# See, https://bugs.archlinux.org/task/31187
_NUMAdisable=y

#enable winesync
_winesync=y

#enable anbox
_use_anbox=

### Running with a 1000HZ
_1k_HZ_ticks=y

### Selecting the ZSTD compression level
# 'ultra' - highest compression ratio
# 'normal' - standard compression ratio
_zstd_level='normal'

### Selecting the ZSTD module compression level
# 'ultra' - highest compression ratio
# 'normal' - standard compression ratio
_zstd_module_level='normal'


# Define pkgname 
if [ -n "$_use_aocc" ]; then
	clear
	msg "AOCC compiler selected"
  pkgbase=linux-aocc
elif [ -n "$_use_clang" ]; then
		clear
	msg "CLANG compiler selected"
  pkgbase=linux-clang
else
  pkgbase=linux
fi

### patches URL
_patchsource="https://raw.githubusercontent.com/ptr1337/kernel-patches/master/5.18"
_patchsource2="https://raw.githubusercontent.com/ptr1337/kernel-patches/master/5.18/misc"
### package data
_minor=9
_main=5.18
_base_name=linux
pkgver=${_main}.${_minor}
pkgrel=1
pkgdesc=${pkgbase}
_srcname=${_base_name}-${pkgver}
url="https://kernel.org/"
arch=(x86_64)
license=(GPL2)
options=('!strip')
makedepends=(
  bc kmod libelf pahole cpio perl tar xz
  xmlto python-sphinx python-sphinx_rtd_theme graphviz imagemagick
  git
)
# Set AOCC compiler enviroment / deps
if [ -n "$_use_aocc" ]; then
	_SET_COMPILER="CC=/opt/aocc/bin/clang CXX=/opt/aocc/bin/clang++ LD=/opt/aocc/bin/ld.lld FC=/opt/aocc/bin/flang"
	makedepends+=(aocc python)
elif [ -n "$_use_clang" ]; then
	_SET_COMPILER="CC=clang CXX=clang++ LD=ld.lld FC=flang"
	makedepends+=(clang llvm lld python)
else
  _SET_COMPILER=""
fi


### Common Sources
source=(
  "${_patchsource2}/0001-clearlinux.patch"
  "${_patchsource}/0001-fixes.patch"
#  "${_patchsource}/0001-cachy.patch"
  "${_patchsource2}/0001-cpu.patch"
  "${_patchsource}/0001-fs-patches.patch"
  "config"
)

#### Variable sources
if [ "${_linux_source}" = "arch" ]; then
	_format=tar.gz
	_linux_url="https://codeload.github.com/archlinux/linux/tar.gz/refs/tags"
	# add kernel to the last array of sources
	source[${#source[@]}]="linux-${pkgver}.${_format}::$_linux_url/v${pkgver}-arch1"
	if [ -n "$_use_anbox" ]; then
		source[${#source[@]}]="${_patchsource}/0002-anbox.patch"
	fi
	_linux_dir="linux-${pkgver}-arch${pkgrel}"
	msg "kernel sources from ARCH"
	sleep 2s
elif [ "${_linux_source}" = "official" ]; then
	_format=tar.xz
	_linux_url="https://cdn.kernel.org/pub/${_srcname%-*}/kernel/v5.x"
	source[${#source[@]}]="${_linux_url}/${_srcname%-*}-${pkgver}.${_format}"
	source[${#source[@]}]="${_patchsource}/0001-arch.patch"
	if [ -n "$_use_anbox" ]; then
		source[${#source[@]}]="${_patchsource}/0002-anbox.patch"
	fi
	_linux_dir="${_srcname%-*}-${pkgver}"
	msg "kernel sources from official KERNEL.ORG"
	sleep 2s
else
		msg "wrong kernel source selected" 2>/dev/null
fi

### cheksums
md5sums=('1305f4a3a9a8878c6fbb08e4fcfb09bd'
         '7381a0a68aec449ee3c81eaed98ee2ba'
         'e36f02bd14c159c14ce112b1728fdf21'
         '6d2c2e2f4230199a9fe3a033a3d886fd'
         'b22aada79ae00466642ba3ba9a094945'
         '2618b318cae428262583e93da670148c')

export KBUILD_BUILD_HOST=archlinux
export KBUILD_BUILD_USER=$pkgbase
export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

prepare() {
  cd $_linux_dir
	
	### Change extraversion for Arch Sources 
if [ "${_linux_source}" = "arch" ]; then
	sed -i 's/EXTRAVERSION = -arch1/EXTRAVERSION = -openstage/g' Makefile
fi	

	### Setting version
  echo "Setting version..."
  scripts/setlocalversion --save-scmversion
  echo "-$pkgrel" > localversion.10-pkgrel
  echo "${pkgbase#linux}" > localversion.20-pkgname


	### Patching sources
  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    echo "Applying patch $src..."
    patch -Np1 < "../$src"
    msg "patch done"
  done
	
	### Setting config
  msg "Setting config..."
  cp ../config .config
	
#############################
### APPLING CONFIGS
#############################
  if [ -n "$_use_cfi" ] && [ -n "$_use_aocc" ] || [ -n "$_use_clang" ]; then
    echo "Enabling CFI"
    scripts/config --enable CONFIG_ARCH_SUPPORTS_CFI_CLANG
    scripts/config --enable CONFIG_CFI_CLANG
  fi

  if [ -n "$_use_aocc" ] || [ -n "$_use_clang" ]; then
    echo "Enable LLVM LTO"
    scripts/config --disable CONFIG_LTO_NONE
  fi


  ### Optionally set tickrate to 1000
  if [ -n "$_1k_HZ_ticks" ]; then
    echo "Setting tick rate to 1k..."
    scripts/config --disable CONFIG_HZ_300
    scripts/config --enable CONFIG_HZ_1000
    scripts/config --set-val CONFIG_HZ 1000
  fi

  ### Optionally disable NUMA for 64-bit kernels only
  # (x86 kernels do not support NUMA)
  if [ -n "$_NUMAdisable" ]; then
    echo "Disabling NUMA from kernel config..."
    scripts/config --disable CONFIG_NUMA
  fi

  ### winesync
  if [ -n "$_winesync" ]; then
    echo "Enable winesync support"
    scripts/config --module CONFIG_WINESYNC
  fi

  ### Selecting the ZSTD module compression level
  if [ "$_zstd_module_level" = "ultra" ]; then
    msg "Enabling highest ZSTD mcompression"
    scripts/config --set-val CONFIG_MODULE_COMPRESS_ZSTD_LEVEL 19
    scripts/config --enable CONFIG_MODULE_COMPRESS_ZSTD_ULTRA
    scripts/config --set-val CONFIG_MODULE_COMPRESS_ZSTD_LEVEL_ULTRA 22
  elif [ "$_zstd_module_level" = "normal" ]; then
    msg "Enabling standard ZSTD compression"
    scripts/config --set-val CONFIG_MODULE_COMPRESS_ZSTD_LEVEL 19
    scripts/config --disable CONFIG_MODULE_COMPRESS_ZSTD_ULTRA
  else
    if [ -n "$_zstd_module_level" ]; then
      error "The value $_zstd_module_level is invalid. Choose the correct one again."
    else
      error "The value is empty. Choose the correct one again."
    fi
    error "Selecting the ZSTD module compression level failed!"
    exit
  fi

# Enable ANBOX
##	if [ -n "$_use_anbox" ]; then
##		msg "Anbox enabled"
##		scripts/config --enable CONFIG_ASHMEM
##		scripts/config --enable CONFIG_ANDROID
##		scripts/config --enable CONFIG_ANDROID_BINDER_IPC
##		scripts/config --enable CONFIG_ANDROID_BINDERFS
##		scripts/config --enable CONFIG_ANDROID_BINDER_DEVICES="binder,hwbinder,vndbinder"
##		scripts/config --enable CONFIG_NTFS3_FS
##	fi

#  echo "Setting schedutil governor..."
#  scripts/config --disable CONFIG_CPU_FREQ_DEFAULT_GOV_SCHEDUTIL
#  scripts/config --enable CONFIG_CPU_FREQ_DEFAULT_GOV_PERFORMANCE
#  scripts/config --enable CONFIG_CPU_FREQ_DEFAULT_GOV_SCHEDUTIL
#  scripts/config --enable CONFIG_CPU_FREQ_GOV_ONDEMAND
#  scripts/config --enable CONFIG_CPU_FREQ_GOV_PERFORMANCE
#  scripts/config --enable CONFIG_CPU_FREQ_GOV_CONSERVATIVE
#  scripts/config --enable CONFIG_CPU_FREQ_GOV_USERSPACE
#  scripts/config --enable CONFIG_CPU_FREQ_GOV_SCHEDUTIL

#############################
#############################	
	
	msg "Applying default config..."
	make $_SET_COMPILER olddefconfig
  make $_SET_COMPILER -s kernelrelease > version
  echo "Prepared $pkgbase version $(<version)"
  
  ### Save configuration file backup
  cp -Tf ./.config "${startdir}/config-${pkgbase}-${pkgver}-${pkgrel}"

}

build() {
  cd $_linux_dir
  make $_SET_COMPILER -j$(nproc) all
  make $_SET_COMPILER htmldocs
}

_package() {
  pkgdesc="The $pkgdesc kernel and modules"
  depends=(coreutils kmod initramfs)
  optdepends=('crda: to set the correct wireless channels of your country'
              'linux-firmware: firmware images needed for some devices')
  provides=(VIRTUALBOX-GUEST-MODULES WIREGUARD-MODULE linux)
  replaces=(virtualbox-guest-modules-arch wireguard-arch)

  cd $_linux_dir
  local kernver="$(<version)"
  local modulesdir="$pkgdir/usr/lib/modules/$kernver"

  echo "Installing boot image..."
  # systemd expects to find the kernel here to allow hibernation
  # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
  install -Dm644 "$(make $_SET_COMPILER -s image_name)" "$modulesdir/vmlinuz"

  # Used by mkinitcpio to name the kernel
  echo "$pkgbase" | install -Dm644 /dev/stdin "$modulesdir/pkgbase"

  echo "Installing modules..."
  make $_SET_COMPILER INSTALL_MOD_PATH="$pkgdir/usr" INSTALL_MOD_STRIP=1 modules_install

  # remove build and source links
  rm "$modulesdir"/{source,build}
}

_package-headers() {
  pkgdesc="Headers and scripts for building modules for the $pkgdesc kernel"
  depends=(pahole)
	provides=(linux-headers)
  cd $_linux_dir
  local builddir="$pkgdir/usr/lib/modules/$(<version)/build"

  echo "Installing build files..."
  install -Dt "$builddir" -m644 .config Makefile Module.symvers System.map \
    localversion.* version vmlinux
  install -Dt "$builddir/kernel" -m644 kernel/Makefile
  install -Dt "$builddir/arch/x86" -m644 arch/x86/Makefile
  cp -t "$builddir" -a scripts

  # required when STACK_VALIDATION is enabled
  install -Dt "$builddir/tools/objtool" tools/objtool/objtool

  # required when DEBUG_INFO_BTF_MODULES is enabled
  install -Dt "$builddir/tools/bpf/resolve_btfids" tools/bpf/resolve_btfids/resolve_btfids

  echo "Installing headers..."
  cp -t "$builddir" -a include
  cp -t "$builddir/arch/x86" -a arch/x86/include
  install -Dt "$builddir/arch/x86/kernel" -m644 arch/x86/kernel/asm-offsets.s

  install -Dt "$builddir/drivers/md" -m644 drivers/md/*.h
  install -Dt "$builddir/net/mac80211" -m644 net/mac80211/*.h

  # https://bugs.archlinux.org/task/13146
  install -Dt "$builddir/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # https://bugs.archlinux.org/task/20402
  install -Dt "$builddir/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "$builddir/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "$builddir/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # https://bugs.archlinux.org/task/71392
  install -Dt "$builddir/drivers/iio/common/hid-sensors" -m644 drivers/iio/common/hid-sensors/*.h

  echo "Installing KConfig files..."
  find . -name 'Kconfig*' -exec install -Dm644 {} "$builddir/{}" \;

  echo "Removing unneeded architectures..."
  local arch
  for arch in "$builddir"/arch/*/; do
    [[ $arch = */x86/ ]] && continue
    echo "Removing $(basename "$arch")"
    rm -r "$arch"
  done

  echo "Removing documentation..."
  rm -r "$builddir/Documentation"

  echo "Removing broken symlinks..."
  find -L "$builddir" -type l -printf 'Removing %P\n' -delete

  echo "Removing loose objects..."
  find "$builddir" -type f -name '*.o' -printf 'Removing %P\n' -delete

  echo "Stripping build tools..."
  local file
  while read -rd '' file; do
    case "$(file -bi "$file")" in
      application/x-sharedlib\;*)      # Libraries (.so)
        strip -v $STRIP_SHARED "$file" ;;
      application/x-archive\;*)        # Libraries (.a)
        strip -v $STRIP_STATIC "$file" ;;
      application/x-executable\;*)     # Binaries
        strip -v $STRIP_BINARIES "$file" ;;
      application/x-pie-executable\;*) # Relocatable binaries
        strip -v $STRIP_SHARED "$file" ;;
    esac
  done < <(find "$builddir" -type f -perm -u+x ! -name vmlinux -print0)

  echo "Stripping vmlinux..."
  strip -v $STRIP_STATIC "$builddir/vmlinux"

  echo "Adding symlink..."
  mkdir -p "$pkgdir/usr/src"
  ln -sr "$builddir" "$pkgdir/usr/src/$pkgbase"
}

_package-docs() {
  pkgdesc="Documentation for the $pkgdesc kernel"
	provides=(linux-docs)
  cd $_linux_dir
  local builddir="$pkgdir/usr/lib/modules/$(<version)/build"

  echo "Installing documentation..."
  local src dst
  while read -rd '' src; do
    dst="${src#Documentation/}"
    dst="$builddir/Documentation/${dst#output/}"
    install -Dm644 "$src" "$dst"
  done < <(find Documentation -name '.*' -prune -o ! -type d -print0)

  echo "Adding symlink..."
  mkdir -p "$pkgdir/usr/share/doc"
  ln -sr "$builddir/Documentation" "$pkgdir/usr/share/doc/$pkgbase"
}

pkgname=("$pkgbase" "$pkgbase-headers" "$pkgbase-docs")
for _p in "${pkgname[@]}"; do
  eval "package_$_p() {
    $(declare -f "_package${_p#$pkgbase}")
    _package${_p#$pkgbase}
  }"
done

# vim:set ts=8 sts=2 sw=2 et:
